# Algoritmos Procesamiento Gráfico

Proyecto realizado en el IDE Netbeans

Look and Feel adaptado al Sistema Operativo

##  Dependencias

[Graficos Estadísticos](https://gitlab.com/JSiapo/MiniPhotoshop/tree/master/GraficosEstadisticos) : Crear libreria


## Operaciones

- [x] Escala de grises
- [x] Negativo
- [x] Binario
- [x] Escalamiento
      - [x] Duplicar tamaño
      - [x] Aumentar N x M
      - [x] Reducir tamaño a la mitad
      - [x] Reducir N x M
- [x] Operaciones Lógicas
      - [x] AND
      - [x] OR
      - [x] XOR
      - [x] NAND
      - [x] NOT
- [x] Operaciones Aritmeticas
      - [x] Adición
      - [x] Sustracción
      - [x] Multiplicación
      - [x] División
- [x] Rotación
      - [x] 90°
      - [x] 180°
- [x] Operaciones Morfológicas
      - [x] Erosión Binaria
      - [x] Dilatación Binaria
      - [x] Apertura Binario
      - [x] Esqueleto
      - [x] Borde
- [x] Filtros
      - [x] Mínimo
      - [x] Máximo
      - [x] Media
      - [x] Mediana