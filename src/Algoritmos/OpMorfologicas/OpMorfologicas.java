/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpMorfologicas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class OpMorfologicas {

    private final Imagen imagen;
    private int[][] estructurante;
    private int[][] erosion;
    private int[][] dilatacion;
    private int[][] open;
    private int[][] close;
    private int[][] skull;
    private int[][] border;
    private final int origenx;
    private final int origeny;
    private int cant_uno_estructurante, cant_unos;
    
    public OpMorfologicas(Imagen imagen) {
        this.imagen = imagen;
        initEstructurante();
        erosion = new int[imagen.getFilas()][imagen.getColumnas()];
        dilatacion = new int[imagen.getFilas()][imagen.getColumnas()];
        origenx = estructurante.length / 2;
        origeny = estructurante[0].length / 2;
        for (int i = 0; i < estructurante.length; i++) {
            for (int j = 0; j < estructurante[0].length; j++) {
                if (estructurante[i][j] == 255) {
                    cant_uno_estructurante++;
                }
            }
        }
        border = new int[imagen.getFilas()][imagen.getColumnas()];
    }

    private void initEstructurante(){
        estructurante = new int[3][3];
        estructurante[0][0] = 0;
        estructurante[0][1] = 255;
        estructurante[0][2] = 0;

        estructurante[1][0] = 255;
        estructurante[1][1] = 255;
        estructurante[1][2] = 255;

        estructurante[2][0] = 0;
        estructurante[2][1] = 255;
        estructurante[2][2] = 0;
    }
    
    public int[][] erosion(int[][] matriz) {
        for (int i = origenx; i < matriz.length - origenx; i++) {
            for (int j = origeny; j < matriz[0].length - origeny; j++) {
                if (estructurante[origenx][origeny] == matriz[i][j]) {
                    cant_unos = 0;
                    for (int k = 0; k < estructurante.length; k++) {
                        for (int l = 0; l < estructurante[0].length; l++) {
                            if (estructurante[k][l] == matriz[k + i - origenx][l + j - origeny] && estructurante[k][l] == 255) {
                                cant_unos++;
                            }
                        }
                    }
                    if (cant_unos == cant_uno_estructurante) {
                        erosion[i][j] = matriz[i][j];
                    }
                } else {
                    erosion[i][j] = 0;
                }
            }
        }

        return erosion;
    }

    public int[][] dilatacion(int[][] matriz) {
        for (int i = origenx; i < matriz.length - origenx; i++) {
            for (int j = origeny; j < matriz[0].length - origeny; j++) {
                if (estructurante[origenx][origeny] == matriz[i][j]) {
                    for (int k = 0; k < estructurante.length; k++) {
                        for (int l = 0; l < estructurante[0].length; l++) {
                            dilatacion[k + i - origenx][l + j - origeny] = estructurante[k][l];
                        }
                    }
                } else {
                    dilatacion[i][j] = 0;
                }
            }
        }
        return dilatacion;
    }

    private boolean buscar_uno(int[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[0].length; j++) {
                if (matriz[i][j] == 255) {
                    return true;
                }
            }
        }
        return false;
    }

    public int[][] apertura() {
        open = erosion(imagen.getBinario());
        open = dilatacion(open);
        return open;
    }

    public int[][] cierre() {
        close = dilatacion(imagen.getBinario());
        close = erosion(close);
        return close;
    }

    public int[][] esqueleto() {
        int[][] skull_back = new int[imagen.getBinario().length][imagen.getBinario()[0].length];
        skull = erosion(imagen.getBinario());
        for (int i = 0; i < imagen.getBinario().length; i++) {
            if (!buscar_uno(skull)) {
                break;
            }
            skull = buscar_uno(skull) ? erosion(skull) : skull_back;
            skull_back = skull;
        }
        skull = skull_back;
        return skull;
    }

    public int[][] borde() {
        border = erosion(imagen.getBinario());
        for (int i = 0; i < imagen.getFilas(); i++) {
            for (int j = 0; j < imagen.getColumnas(); j++) {
                border[i][j] = imagen.getBinario()[i][j] - border[i][j];
            }
        }
        return border;
    }

}
