/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos;

import Algoritmos.Filtros.Filtros;
import Algoritmos.Histograma.HistogramaRojo;
import Algoritmos.OpAritemticas.Division;
import Algoritmos.OpAritemticas.Multiplicacion;
import Algoritmos.OpAritemticas.Resta;
import Algoritmos.OpAritemticas.Suma;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
import java.awt.geom.*;
import Algoritmos.Zoom.*;
import Algoritmos.OpLogicas.*;
import Algoritmos.Rotar.*;
import Algoritmos.Histograma.*;
import Algoritmos.OpMorfologicas.OpMorfologicas;
import Algoritmos.Filtros.*;

public class Imagen extends Component{
    //------------------------Atributos---------------------------------------//
    BufferedImage img;
    int [][] matrizImg;
    int [][] matrizImg_R;
    int [][] matrizImg_G;
    int [][] matrizImg_B;
    int columnas;
    int filas;
    int umbral=128;
    int [][]binario;

    int [][] matrizImg2;
    int [][] matrizImgR2;
    int [][] matrizImgG2;
    int [][] matrizImgB2;

    DefaultCategoryDataset ds_r;
    DefaultCategoryDataset ds_g;
    DefaultCategoryDataset ds_b;
    int r[] = new int[256];
    int g[] = new int[256];
    int b[] = new int[256];

    //<editor-fold defaultstate="collapsed" desc=" funciones necesarias">
    private void binarizar(){
        binario=null;
        binario=new int[matrizImg.length][matrizImg[0].length];

        for (int j = 0; j < matrizImg.length; j++) {
            for (int i = 0; i < matrizImg[0].length; i++) {
                if (matrizImg[j][i]>umbral) {
                    binario[j][i]=255;
                }
                else{
                    binario[j][i]=0;
                }
            }
        }
    }
    public Imagen(String nombreImagen){
        try{
            img = ImageIO.read(new File(nombreImagen));
            convertirImagenAMatriz(img);
            binarizar();
        }catch (IOException e){
            System.out.println("Error: " + e);
        }
    }

    public Imagen(int [][]matriz){
        convertirMatrizAImagen(matriz);
    }

    public Imagen(int [][]Red,int[][]Green,int[][]Blue){
        convertirMatrizAImagenColor(Red,Green,Blue);
    }

    public void convertirImagenAMatriz(BufferedImage img){
        filas = img.getHeight();
        columnas = img.getWidth();
        matrizImg = new int [filas][columnas];
        matrizImg_R = new int [filas][columnas];
        matrizImg_G = new int [filas][columnas];
        matrizImg_B = new int [filas][columnas];
        int r;
        int g;
        int b;

        WritableRaster raster = img.getRaster();
        int numBandas=raster.getNumBands();

        for (int i=0;i<filas;i++){
            for(int j=0;j<columnas;j++){
                if (numBandas==3){
                    r=raster.getSample(j,i,0);
                    g=raster.getSample(j,i,1);
                    b=raster.getSample(j,i,2);
                    matrizImg[i][j]=(r+g+b)/3;
                    matrizImg_R[i][j]=r;
                    matrizImg_G[i][j]=g;
                    matrizImg_B[i][j]=b;
                }
                if (numBandas==1){
                    matrizImg[i][j]=raster.getSample(j,i,0);
                    matrizImg_R[i][j]=255;
                    matrizImg_G[i][j]=255;
                    matrizImg_B[i][j]=255;
                }
            }
        }
    }

    public void convertirMatrizAImagen(int [][] matriz){
        int alto = matriz.length;
        int ancho = matriz[0].length;
        img = new BufferedImage(ancho,alto,BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster wr = img.getRaster();

        for (int i=0;i<alto;i++){
            for(int j=0;j<ancho;j++){
                wr.setSample(j,i,0,matriz[i][j]);
            }
        }

        img.setData(wr);
    }

    public void convertirMatrizAImagenColor(int [][] matrizR,int [][]matrizG,int [][]matrizB ){
        int alto = matrizR.length;
        int ancho = matrizR[0].length;
        img = new BufferedImage(ancho,alto,BufferedImage.TYPE_INT_RGB);
        WritableRaster wr = img.getRaster(); 
        for (int i=0;i<alto;i++){
            for(int j=0;j<ancho;j++){
                wr.setSample(j,i,0,matrizR[i][j]);
                wr.setSample(j,i,1,matrizG[i][j]);
                wr.setSample(j,i,2,matrizB[i][j]);
            }
        }
        img.setData(wr);
    }

    public void convertirMatrizAImagenColorNegativo(int [][] matrizR,int [][]matrizG,int [][]matrizB ){
        int alto = matrizR.length;
        int ancho = matrizR[0].length;
        img = new BufferedImage(ancho,alto,BufferedImage.TYPE_INT_RGB);
        WritableRaster wr = img.getRaster();
        for (int i=0;i<alto;i++){
            for(int j=0;j<ancho;j++){
                wr.setSample(j,i,0,255-matrizR[i][j]);
                wr.setSample(j,i,1,255-matrizG[i][j]);
                wr.setSample(j,i,2,255-matrizB[i][j]);
            }
        }

        img.setData(wr);
    }

    public void guardarImagen(int [][]matriz, String path){
        BufferedImage img = new BufferedImage(matriz[0].length,matriz.length,BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster wr = img.getRaster();
        for (int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[0].length;j++){
                wr.setSample(j,i,0,matriz[i][j]);
            }
        }
        img.setData(wr);
        try{
            ImageIO.write(img, "JPG", new File(path+".jpg"));
        }catch(IOException e){

        }
    }

    public void guardarImagen(int [][]matriz_R, int [][]matriz_G, int [][]matriz_B,String path){
        BufferedImage imgn = new BufferedImage(matriz_R[0].length,matriz_R.length,BufferedImage.TYPE_INT_RGB);
        WritableRaster wr = imgn.getRaster();
        for (int i=0;i<matriz_R.length;i++){
            for(int j=0;j<matriz_R[0].length;j++){
                wr.setSample(j,i,0,matriz_R[i][j]);
                wr.setSample(j,i,1,matriz_G[i][j]);
                wr.setSample(j,i,2,matriz_B[i][j]);
            }
        }
        img.setData(wr);
        try{
            ImageIO.write(img, "JPG", new File(path+".jpg"));
        }catch(IOException e){

        }
    }
    //</editor-fold>

    public void negativo(Imagen imagen){
        Negativo n=new Negativo(imagen);
        ventana(n.getNegativo_R(),n.getNegativo_G(),n.getNegativo_B(), "Negativo");
    }
    
    public void Gris(Imagen imagen) {
        ventana(imagen.matrizImg, "Gris");
    }
    
    public void Binario(Imagen imagen) {
        ventana(binario, "Binario");
    }
    
    public void Zoom_duplicar(Imagen imagen){
        ZoomDuplicar d= new ZoomDuplicar(imagen);
        ventana(d.getZoom_R(),d.getZoom_G(),d.getZoom_B(),"Duplicado");
    }
    
    public void Zoom_aumentar(Imagen imagen,int n,int m){
        Zoom_aumentar_nm d= new Zoom_aumentar_nm(imagen, n, m);
        ventana(d.getZoom_R(),d.getZoom_G(),d.getZoom_B(),"Aumentado "+n+"x"+m);
    }
    
    public void Zoom_mitad(Imagen imagen){
        Zoom_mitad d= new Zoom_mitad(imagen);
        ventana(d.getZoom_R(),d.getZoom_G(),d.getZoom_B(),"Mitad");
    }
    
    public void Zoom_reducir(Imagen imagen,int n,int m){
        Zoom_menos_nm d= new Zoom_menos_nm(imagen, n, m);
        ventana(d.getZoom_R(),d.getZoom_G(),d.getZoom_B(),"Reducir "+n+"x"+m);
    }
    
    public void And(Imagen imagenUno, Imagen imagenDos){
        And opL= new And(imagenUno, imagenDos);
        ventana(opL.getOL_R(), opL.getOL_G(), opL.getOL_B(), "And");
    }
    
    public void Nand(Imagen imagenUno, Imagen imagenDos){
        Nand opL= new Nand(imagenUno, imagenDos);
        ventana(opL.getOL_R(), opL.getOL_G(), opL.getOL_B(), "Nand");
    }
    
    public void Or(Imagen imagenUno, Imagen imagenDos){
        Or opL= new Or(imagenUno, imagenDos);
        ventana(opL.getOL_R(), opL.getOL_G(), opL.getOL_B(), "Or");
    }
    
    public void Xor(Imagen imagenUno, Imagen imagenDos){
        Xor opL= new Xor(imagenUno, imagenDos);
        ventana(opL.getOL_R(), opL.getOL_G(), opL.getOL_B(), "Xor");
    }
    
    public void Not(Imagen imagen){
        Not opL=new Not(imagen);
        ventana(opL.getOL_R(), opL.getOL_G(), opL.getOL_B(), "Not");
    }
    
    public void Suma(Imagen imagenUno, Imagen imagenDos){
        Suma opA= new Suma(imagenUno, imagenDos,1);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(),"Suma Fars");
        
        opA= new Suma(imagenUno, imagenDos,2);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(),"Suma 2");
    }
    
    public void Resta(Imagen imagenUno, Imagen imagenDos){
        Resta opA=new Resta(imagenUno, imagenDos, 1);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Resta Valor absoluto");
        opA=new Resta(imagenUno, imagenDos, 2);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Resta Formula");
        opA=new Resta(imagenUno, imagenDos, 3);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Resta Cero");
    }
    
    public void Multiplicacion(Imagen imagenUno, Imagen imagenDos){
        Multiplicacion opA=new Multiplicacion(imagenUno, imagenDos);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Multiplicacion");
    }
    
    public void Division(Imagen imagenUno, Imagen imagenDos){
        Division opA=new Division(imagenUno, imagenDos, 1);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Division 1");
        opA=new Division(imagenUno, imagenDos, 2);
        ventana(opA.getOP_R(),opA.getOP_G(),opA.getOP_B(), "Division 2");
    }
    
    public void Rotar90(Imagen imagen){
        Rotar90 r=new Rotar90(imagen);
        ventana(r.getR_R(),r.getR_G(),r.getR_B(),"Rotar 90°");
    }
    
    public void Rotar180(Imagen imagen){
        Rotar180 r=new Rotar180(imagen);
        ventana(r.getR_R(),r.getR_G(),r.getR_B(),"Rotar 180°");
    }
    
    public void HistogramaRojo(Imagen imagen){
        HistogramaRojo h=new HistogramaRojo(imagen);
        ventana(h.getDs_r(), "Histograma Rojo");
    }
    
    public void HistogramaVerde(Imagen imagen){
        HistogramaVerde h=new HistogramaVerde(imagen);
        ventana(h.getDs_g(), "Histograma Verde");
    }
    
    public void HistogramaAzul(Imagen imagen){
        HistogramaAzul h=new HistogramaAzul(imagen);
        ventana(h.getDs_b(), "Histograma Azul");
    }
    
    public void HistogramaGris(Imagen imagen){
        HistogramaGris h=new HistogramaGris(imagen);
        ventana(h.getDs(), "Histograma Gris");
    }
    
    //Funciones Morfologia matemática
    public void erosion_binaria(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.erosion(binario),"Erosion Binaria");
    }

    public void dilatacion_binaria(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.dilatacion(binario),"Dilatación Binaria");
    }


    public void apertura_binario(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.apertura(),"Apertura Binaria");
    }


    public void cierre_binario(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.cierre(),"Cierre Binaria");
    }

    public void esqueleto(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.esqueleto(),"Esqueleto");
    }

    public void borde(Imagen imagen){
        OpMorfologicas op=new OpMorfologicas(imagen);
        ventana(binario,"Binario");
        ventana(op.borde(),"Borde");
    }
    
    //Filtros
    public void filtroMediana(Imagen imagen, int tamMascara){
        Mediana f=new Mediana(imagen,tamMascara);
        ventana(f.getFiltro_r(),f.getFiltro_g(),f.getFiltro_b(),"Filtro mediana");
    }

    public void filtroMinimo(Imagen imagen, int tamMascara){
        Minimo f=new Minimo(imagen,tamMascara);
        ventana(f.getFiltro_r(),f.getFiltro_g(),f.getFiltro_b(),"Filtro minimo");
    }

    public void filtroMaximo(Imagen imagen, int tamMascara){
        Maximo f=new Maximo(imagen,tamMascara);
        ventana(f.getFiltro_r(),f.getFiltro_g(),f.getFiltro_b(),"Filtro maximo");
    }

    public void filtroMedia(Imagen imagen, int tamMascara){
        Media f=new Media(imagen,tamMascara);
        ventana(f.getFiltro_r(),f.getFiltro_g(),f.getFiltro_b(),"Filtro media");
    }
    
    // <editor-fold defaultstate="collapsed" desc="Ventanas">
    @Override
    public void paint(Graphics g) {
        int x=0;
        int y=200;  //valor estandar de y
        int p=0;
        int q=0;

        x = img.getWidth(null);// * y/img.getHeight(null);
        y = img.getHeight(null);// * y/img.getHeight(null);

        g.drawImage(img, 100, 100, null);
        g.drawImage(img, 0, 0, x, y, 0, 0, img.getWidth(null), img.getHeight(null), null);
        Graphics2D g2 = (Graphics2D) g;

        for(int i=0;i<1;i++){
            g2.setStroke(new BasicStroke(2.0f));
            p=(int)Math.ceil(200*Math.random());
            q=(int)Math.ceil(200*Math.random());
            g2.draw(new Rectangle2D.Double(p, q,10,10));
            g2.setStroke(new BasicStroke(3.0f));
            p=(int)Math.ceil(200*Math.random());
            q=(int)Math.ceil(200*Math.random());
            g2.draw(new Ellipse2D.Double(p, q,10,10));
        }
    }

    @Override
    public Dimension getPreferredSize(){
        int x=0;
        int y=200;
        if (img == null){
            return new Dimension(100,100);
        }else{
            x = img.getWidth(null) ;//* y/img.getHeight(null);
            y = img.getHeight(null);// * y/img.getHeight(null);
            return new Dimension(x, y);
        }
    }

    public void ventana(DefaultCategoryDataset d, String nombre) {
        JFreeChart jf = ChartFactory.createBarChart3D(nombre, "intensidad", "cantidad", d, PlotOrientation.VERTICAL, true, true, true);
        ChartFrame f = new ChartFrame("Histograma", jf);
        f.setSize(1000, 600);
        f.setLocationRelativeTo(null);
        f.setIconImage(new ImageIcon(getClass().getResource("/imagen/icon.png")).getImage());
        f.setVisible(true);
    }

    /*Coloca una imagen en una ventana*/
    public void ventana(int matriz[][],String title){
        Imagen imagen=new Imagen(matriz);
        try{
            JFrame f = new JFrame(title);
            f.addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e){
                }
            });
            f.add(imagen);
            f.pack();
            f.setVisible(true);
            f.repaint();
            f.setLocationRelativeTo(null);
        }catch (Exception e){

        }
    }

    public void ventana(int matrizR[][],int matrizG[][],int matrizB[][],String title){
        Imagen imagen=new Imagen(matrizR,matrizG,matrizB);
        try{
            JFrame f = new JFrame(title);
            f.addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e){
                }
            });

            f.add(imagen);
            f.pack();
            f.setVisible(true);
            f.repaint();
            f.setLocationRelativeTo(null);
        }catch (Exception e){

        }
    }
    
    // </editor-fold>

    public BufferedImage getImg() {
        return img;
    }

    public int[][] getMatrizImg() {
        return matrizImg;
    }

    public int[][] getMatrizImg_R() {
        return matrizImg_R;
    }

    public int[][] getMatrizImg_G() {
        return matrizImg_G;
    }

    public int[][] getMatrizImg_B() {
        return matrizImg_B;
    }

    public int getColumnas() {
        return columnas;
    }

    public int getFilas() {
        return filas;
    }

    public int getUmbral() {
        return umbral;
    }

    public int[][] getBinario() {
        return binario;
    }
}
