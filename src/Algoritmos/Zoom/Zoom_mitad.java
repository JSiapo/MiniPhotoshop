/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Zoom;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Zoom_mitad extends Zoom {

    public Zoom_mitad(Imagen imagen) {
        super(imagen);
        mitad(imagen);
    }

    private void mitad(Imagen imagen) {
        int alto = (imagen.getFilas()) / 2;
        int ancho = (imagen.getColumnas()) / 2;

        Zoom_R = null;
        Zoom_G = null;
        Zoom_B = null;

        Zoom_R = new int[imagen.getFilas() / 2][imagen.getColumnas() / 2];
        Zoom_G = new int[imagen.getFilas() / 2][imagen.getColumnas() / 2];
        Zoom_B = new int[imagen.getFilas() / 2][imagen.getColumnas() / 2];

        int jp, ip;

        for (int j = 0; j < ancho; j++) {
            jp = 2 * j;
            for (int i = 0; i < alto; i++) {
                ip = 2 * i;
                Zoom_R[i][j] = imagen.getMatrizImg_R()[ip][jp];
                Zoom_G[i][j] = imagen.getMatrizImg_G()[ip][jp];
                Zoom_B[i][j] = imagen.getMatrizImg_B()[ip][jp];
        }
    }
}
}
