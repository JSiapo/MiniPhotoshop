/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Zoom;

import Algoritmos.Imagen;
import java.awt.image.WritableRaster;

/**
 *
 * @author jose
 */
public class ZoomDuplicar extends Zoom{
    
    public ZoomDuplicar(Imagen imagen) {
        super(imagen);
        duplicar(imagen);
    }
    
    private void duplicar(Imagen imagen){
        int alto = imagen.getFilas();
        int ancho = imagen.getColumnas();
        img=imagen.getImg();
        WritableRaster raster = img.getRaster();
        int numeroCanal = raster.getNumBands();

        int jp, ip;
        Zoom = null;
        Zoom_R = null;
        Zoom_G = null;
        Zoom_B = null;

        Zoom = new int[imagen.getFilas() * 2][imagen.getColumnas() * 2];
        Zoom_R = new int[imagen.getFilas() * 2][imagen.getColumnas() * 2];
        Zoom_G = new int[imagen.getFilas() * 2][imagen.getColumnas() * 2];
        Zoom_B = new int[imagen.getFilas() * 2][imagen.getColumnas() * 2];

        for (int j = 0; j < ancho; j++) {
            jp = 2 * j;
            for (int i = 0; i < alto; i++) {
                ip = 2 * i;

                if (numeroCanal == 3) {
                    Zoom_R[ip][jp] = imagen.getMatrizImg_R()[i][j];
                    Zoom_R[ip][jp + 1] = imagen.getMatrizImg_R()[i][j];
                    Zoom_R[ip + 1][jp] = imagen.getMatrizImg_R()[i][j];
                    Zoom_R[ip + 1][jp + 1] = imagen.getMatrizImg_R()[i][j];

                    Zoom_G[ip][jp] = imagen.getMatrizImg_G()[i][j];
                    Zoom_G[ip][jp + 1] = imagen.getMatrizImg_G()[i][j];
                    Zoom_G[ip + 1][jp] = imagen.getMatrizImg_G()[i][j];
                    Zoom_G[ip + 1][jp + 1] = imagen.getMatrizImg_G()[i][j];

                    Zoom_B[ip][jp] = imagen.getMatrizImg_B()[i][j];
                    Zoom_B[ip][jp + 1] = imagen.getMatrizImg_B()[i][j];
                    Zoom_B[ip + 1][jp] = imagen.getMatrizImg_B()[i][j];
                    Zoom_B[ip + 1][jp + 1] = imagen.getMatrizImg_B()[i][j];
                }

                if (numeroCanal == 1) {
                    Zoom[ip][jp] = imagen.getMatrizImg()[i][j];
                    Zoom[ip][jp + 1] = imagen.getMatrizImg()[i][j];
                    Zoom[ip + 1][jp] = imagen.getMatrizImg()[i][j];
                    Zoom[ip + 1][jp + 1] = imagen.getMatrizImg()[i][j];
                }
            }
        }
    }
}
