/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Zoom;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Zoom_menos_nm extends Zoom{
    
    public Zoom_menos_nm(Imagen imagen, int n, int m) {
        super(imagen, n, m);
        reducir(imagen, n, m);
    }
    
    private void reducir(Imagen imagen, int n, int m){
        int alto = imagen.getFilas() / m;
        int ancho = imagen.getColumnas() / n;

        Zoom_R = null;
        Zoom_G = null;
        Zoom_B = null;

        Zoom_R = new int[alto][ancho];
        Zoom_G = new int[alto][ancho];
        Zoom_B = new int[alto][ancho];

        int jp, ip;

        for (int j = 0; j < ancho; j++) {
            jp = j;
            for (int i = 0; i < alto; i++) {
                ip = i;
                for (int canal = 0; canal < 3; canal++) {
                    if (canal == 0) {
                        Zoom_R[ip][jp] = imagen.getMatrizImg_R()[i * m][j * n];
                    }
                    if (canal == 1) {
                        Zoom_G[ip][jp] = imagen.getMatrizImg_G()[i * m][j * n];
                    }
                    if (canal == 2) {
                        Zoom_B[ip][jp] = imagen.getMatrizImg_B()[i * m][j * n];
                    }
                }
            }
        }
    }
}
