/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Zoom;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Zoom_aumentar_nm extends Zoom{
    
    public Zoom_aumentar_nm(Imagen imagen, int n, int m) {
        super(imagen, n, m);
        aumentar_nm(imagen, n, m);
    }
    
    private void aumentar_nm(Imagen imagen, int n, int m){
        Zoom = null;
        Zoom_R = null;
        Zoom_G = null;
        Zoom_B = null;

        Zoom = new int[imagen.getFilas() * m][imagen.getColumnas() * n];
        Zoom_R = new int[imagen.getFilas() * m][imagen.getColumnas() * n];
        Zoom_G = new int[imagen.getFilas() * m][imagen.getColumnas() * n];
        Zoom_B = new int[imagen.getFilas() * m][imagen.getColumnas() * n];

        int jp, ip;

        for (int j = 0; j < imagen.getColumnas(); j++) {
            jp = n * j;
            for (int i = 0; i < imagen.getFilas(); i++) {
                ip = m * i;
                for (int canal = 0; canal < 3; canal++) {
                    if (canal == 0) {
                        for (int ai = 0; ai < n; ai++) {
                            for (int ay = 0; ay < m; ay++) {
                                Zoom_R[ip + ay][jp + ai] = imagen.getMatrizImg_R()[i][j];
                            }
                        }
                    }
                    if (canal == 1) {
                        for (int ai = 0; ai < n; ai++) {
                            for (int ay = 0; ay < m; ay++) {
                                Zoom_G[ip + ay][jp + ai] = imagen.getMatrizImg_G()[i][j];
                            }
                        }
                    }
                    if (canal == 2) {
                        for (int ai = 0; ai < n; ai++) {
                            for (int ay = 0; ay < m; ay++) {
                                Zoom_B[ip + ay][jp + ai] = imagen.getMatrizImg_B()[i][j];
                            }
                        }
                    }
                }
            }
        }
    }
}
