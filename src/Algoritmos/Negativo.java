/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author jose
 */
public class Negativo {
    private int[][] Negativo_R;
    private int[][] Negativo_G;
    private int[][] Negativo_B;

    public Negativo(Imagen imagen) {
        int alto = imagen.getFilas();
        int ancho = imagen.getColumnas();

        Negativo_R = null;
        Negativo_G = null;
        Negativo_B = null;

        Negativo_R = new int[alto][ancho];
        Negativo_G = new int[alto][ancho];
        Negativo_B = new int[alto][ancho];

        for (int j = 0; j < alto; j++) {
            for (int i = 0; i < ancho; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        Negativo_R[j][i] = 255 - imagen.matrizImg_R[j][i];
                    }
                    if (c == 1) {
                        Negativo_G[j][i] = 255 - imagen.matrizImg_G[j][i];
                    }
                    if (c == 2) {
                        Negativo_B[j][i] = 255 - imagen.matrizImg_B[j][i];
                    }
                }
            }
        }
    }

    public int[][] getNegativo_R() {
        return Negativo_R;
    }

    public int[][] getNegativo_G() {
        return Negativo_G;
    }

    public int[][] getNegativo_B() {
        return Negativo_B;
    }
}
