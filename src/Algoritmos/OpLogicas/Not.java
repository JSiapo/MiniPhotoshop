/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpLogicas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Not extends OpLogicas{
    
    public Not(Imagen imagenUno) {
        super(imagenUno);
        not(imagenUno);
    }
    
    private void not(Imagen imagenUno){
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();
        int temp;

        OL_R = null;
        OL_G = null;
        OL_B = null;

        OL_R = new int[altoImgUno][anchoImgUno];
        OL_G = new int[altoImgUno][anchoImgUno];
        OL_B = new int[altoImgUno][anchoImgUno];

        for (int j = 0; j < altoImgUno; j++) {
            for (int i = 0; i < anchoImgUno; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {

                        temp = ~(imagenUno.getMatrizImg_R()[j][i]);
                        if (temp >= 255) {
                            OL_R[j][i] = 255;
                        } else {
                            OL_R[j][i] = temp;
                        }
                    }
                    if (c == 1) {
                        temp = ~(imagenUno.getMatrizImg_G()[j][i]);
                        if (temp >= 255) {
                            OL_G[j][i] = 255;
                        } else {
                            OL_G[j][i] = temp;
                        }
                    }
                    if (c == 2) {
                        temp = ~(imagenUno.getMatrizImg_B()[j][i]);
                        if (temp >= 255) {
                            OL_B[j][i] = 255;
                        } else {
                            OL_B[j][i] = temp;
                        }
                    }
                }
            }
        }
    }
}
