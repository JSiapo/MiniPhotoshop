/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpLogicas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class And extends OpLogicas{

    public And(Imagen imagenUno, Imagen imagenDos) {
        super(imagenUno, imagenDos);
        and(imagenUno, imagenDos);
    }
    
    private void and(Imagen imagenUno,Imagen imagenDos){
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OL_R = null;
        OL_G = null;
        OL_B = null;

        OL_R = new int[altoNuevo][anchoNuevo];
        OL_G = new int[altoNuevo][anchoNuevo];
        OL_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {

                        temp = imagenUno.getMatrizImg_R()[j][i] & imagenDos.getMatrizImg_R()[j][i];
                        if (temp >= 255) {
                            OL_R[j][i] = temp;
                        } else {
                            OL_R[j][i] = temp;
                        }
                    }
                    if (c == 1) {
                        temp = imagenUno.getMatrizImg_G()[j][i] & imagenDos.getMatrizImg_G()[j][i];
                        if (temp >= 255) {
                            OL_G[j][i] = temp;
                        } else {
                            OL_G[j][i] = temp;
                        }
                    }
                    if (c == 2) {
                        temp = imagenUno.getMatrizImg_B()[j][i] & imagenDos.getMatrizImg_B()[j][i];
                        if (temp >= 255) {
                            OL_B[j][i] = temp;
                        } else {
                            OL_B[j][i] = temp;
                        }
                    }
                }
            }
        }
    }
}
