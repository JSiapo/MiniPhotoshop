/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Histograma;

import Algoritmos.Imagen;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author jose
 */
public class Histograma {
    protected DefaultCategoryDataset ds_r;
    protected DefaultCategoryDataset ds_g;
    protected DefaultCategoryDataset ds_b;
    protected DefaultCategoryDataset ds;
    
    protected int altoImgUno;
    protected int anchoImgUno;
    protected int temp, aux = 0;
    
    protected int cantidades[] = new int[256];

    public Histograma(Imagen imagen) {
    }

    public DefaultCategoryDataset getDs_r() {
        return ds_r;
    }

    public DefaultCategoryDataset getDs_g() {
        return ds_g;
    }

    public DefaultCategoryDataset getDs_b() {
        return ds_b;
    }

    public DefaultCategoryDataset getDs() {
        return ds;
    }
    
    
}
