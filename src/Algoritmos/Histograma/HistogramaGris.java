/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Histograma;

import Algoritmos.Imagen;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author jose
 */
public class HistogramaGris extends Histograma{
    
    public HistogramaGris(Imagen imagen) {
        super(imagen);
        altoImgUno = imagen.getFilas();
        anchoImgUno = imagen.getColumnas();
        histograma(imagen);
    }
    
    private void histograma(Imagen imagen) {
        ds = new DefaultCategoryDataset();
        for (int j = 0; j < altoImgUno; j++) {
            for (int i = 0; i < anchoImgUno; i++) {
                temp = imagen.getMatrizImg()[j][i];
                aux = cantidades[temp];
                aux++;
                cantidades[temp] = aux;
                ds.addValue(aux, "" + (temp), "" + (temp));
            }
        }
    }
    
}
