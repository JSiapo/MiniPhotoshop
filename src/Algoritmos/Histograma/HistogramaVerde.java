/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Histograma;

import Algoritmos.Imagen;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author jose
 */
public class HistogramaVerde extends Histograma{
    
    public HistogramaVerde(Imagen imagen) {
        super(imagen);
        altoImgUno = imagen.getFilas();
        anchoImgUno = imagen.getColumnas();
        histograma(imagen);
    }
    
    private void histograma(Imagen imagen) {
        ds_g = new DefaultCategoryDataset();
        for (int j = 0; j < altoImgUno; j++) {
            for (int i = 0; i < anchoImgUno; i++) {
                temp = imagen.getMatrizImg_G()[j][i];
                aux = cantidades[temp];
                aux++;
                cantidades[temp] = aux;
                ds_g.addValue(aux, "" + (temp), "" + (temp));
            }
        }
    }
}
