/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Histograma;

import Algoritmos.Imagen;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author jose
 */
public class HistogramaAzul extends Histograma{
    
    public HistogramaAzul(Imagen imagen) {
        super(imagen);
        altoImgUno = imagen.getFilas();
        anchoImgUno = imagen.getColumnas();
        histograma(imagen);
    }
    
    private void histograma(Imagen imagen) {
        ds_b = new DefaultCategoryDataset();
        for (int j = 0; j < altoImgUno; j++) {
            for (int i = 0; i < anchoImgUno; i++) {
                temp = imagen.getMatrizImg_B()[j][i];
                aux = cantidades[temp];
                aux++;
                cantidades[temp] = aux;
                ds_b.addValue(aux, "" + (temp), "" + (temp));
            }
        }
    }
}
