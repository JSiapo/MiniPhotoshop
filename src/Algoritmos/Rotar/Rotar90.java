/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Rotar;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Rotar90 extends Rotar{
    
    public Rotar90(Imagen imagen) {
        super(imagen);
        rotar90(imagen);
    }
    private void rotar90(Imagen imagen) {
        int altoI = imagen.getFilas();
        int anchoI = imagen.getColumnas();

        R = null;
        R_R = null;
        R_G = null;
        R_B = null;

        R = new int[anchoI][altoI];
        R_R = new int[anchoI][altoI];
        R_G = new int[anchoI][altoI];
        R_B = new int[anchoI][altoI];

        int ip;

        for (int i = 0; i < anchoI; i++) {
            ip = (anchoI - 1) - i;
            for (int j = 0; j < altoI; j++) {
                for (int canal = 0; canal < 3; canal++) {
                    if (canal == 0) {
                        R_R[i][j] = imagen.getMatrizImg_R()[j][ip];
                    }
                    if (canal == 1) {
                        R_G[i][j] = imagen.getMatrizImg_G()[j][ip];
                    }
                    if (canal == 2) {
                        R_B[i][j] = imagen.getMatrizImg_B()[j][ip];
                    }
                }
            }
        }
    }
}
