/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Rotar;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Rotar180 extends Rotar{
    
    public Rotar180(Imagen imagen) {
        super(imagen);
        rotar180(imagen);
    }
    private void rotar180(Imagen imagen) {
        int altoI = imagen.getFilas();
        int anchoI = imagen.getColumnas();

        R = null;
        R_R = null;
        R_G = null;
        R_B = null;

        R = new int[altoI][anchoI];
        R_R = new int[altoI][anchoI];
        R_G = new int[altoI][anchoI];
        R_B = new int[altoI][anchoI];

        int ip, jp;

        for (int i = 0; i < altoI; i++) {
            ip = (altoI - 1) - i;
            for (int j = 0; j < anchoI; j++) {
                jp = (anchoI - 1) - j;
                for (int canal = 0; canal < 3; canal++) {
                    if (canal == 0) {
                        R_R[i][j] = imagen.getMatrizImg_R()[ip][jp];
                    }
                    if (canal == 1) {
                        R_G[i][j] = imagen.getMatrizImg_G()[ip][jp];
                    }
                    if (canal == 2) {
                        R_B[i][j] = imagen.getMatrizImg_B()[ip][jp];
                    }
                }
            }
        }

    }
}
