/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpAritemticas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Resta extends OpAritmeticas{
    
    public Resta(Imagen imagenUno, Imagen imagenDos, int tipo) {
        super(imagenUno, imagenDos);
        switch (tipo) {
            case 1:
                restaImagenes1(imagenUno, imagenDos);
                break;
            case 2:
                restaImagenes2(imagenUno, imagenDos);
                break;
            default:
                restaImagenes3(imagenUno, imagenDos);
                break;
        }
    }
    private void restaImagenes1(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        temp = Math.abs(imagenUno.getMatrizImg_R()[j][i] - imagenDos.getMatrizImg_R()[j][i]);
                        OP_R[j][i] = temp;
                    }
                    if (c == 1) {
                        temp = Math.abs(imagenUno.getMatrizImg_G()[j][i] - imagenDos.getMatrizImg_G()[j][i]);
                        OP_G[j][i] = temp;
                    }
                    if (c == 2) {
                        temp = Math.abs(imagenUno.getMatrizImg_B()[j][i] - imagenDos.getMatrizImg_B()[j][i]);
                        OP_B[j][i] = temp;
                    }
                }
            }
        }

        
    }

    private void restaImagenes2(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        temp = (int) ((255 / 2) + ((imagenUno.getMatrizImg_R()[j][i] - imagenDos.getMatrizImg_R()[j][i]) / 2));
                        OP_R[j][i] = temp;
                    }
                    if (c == 1) {
                        temp = (int) ((255 / 2) + ((imagenUno.getMatrizImg_G()[j][i] - imagenDos.getMatrizImg_G()[j][i]) / 2));
                        OP_G[j][i] = temp;
                    }
                    if (c == 2) {
                        temp = (int) ((255 / 2) + ((imagenUno.getMatrizImg_B()[j][i] - imagenDos.getMatrizImg_B()[j][i]) / 2));
                        OP_B[j][i] = temp;
                    }
                }
            }
        }
        

    }

    private void restaImagenes3(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {

                        temp = imagenUno.getMatrizImg_R()[j][i] - imagenDos.getMatrizImg_R()[j][i];
                        if (temp <= 0) {
                            OP_R[j][i] = 0;
                        } else {
                            OP_R[j][i] = temp;
                        }
                    }
                    if (c == 1) {
                        temp = imagenUno.getMatrizImg_G()[j][i] - imagenDos.getMatrizImg_G()[j][i];
                        if (temp <= 0) {
                            OP_G[j][i] = 0;
                        } else {
                            OP_G[j][i] = temp;
                        }
                    }
                    if (c == 2) {
                        temp = imagenUno.getMatrizImg_B()[j][i] - imagenDos.getMatrizImg_B()[j][i];
                        if (temp <= 0) {
                            OP_B[j][i] = 0;
                        } else {
                            OP_B[j][i] = temp;
                        }
                    }
                }
            }
        }
        
    }
}
