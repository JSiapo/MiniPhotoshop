/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpAritemticas;

import Algoritmos.Imagen;
import java.awt.image.BufferedImage;

/**
 *
 * @author jose
 */
public class OpAritmeticas {
    protected BufferedImage img;
    protected int [][]OP_R;
    protected int [][]OP_G;
    protected int [][]OP_B;
    protected int [][]OP;

    public OpAritmeticas(Imagen imagenUno, Imagen imagenDos) {
    }

    public int[][] getOP_R() {
        return OP_R;
    }

    public int[][] getOP_G() {
        return OP_G;
    }

    public int[][] getOP_B() {
        return OP_B;
    }

    public int[][] getOP() {
        return OP;
    }
    
    
}
