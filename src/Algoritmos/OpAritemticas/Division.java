/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpAritemticas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Division extends OpAritmeticas{
    
    public Division(Imagen imagenUno, Imagen imagenDos, int tipo) {
        super(imagenUno, imagenDos);
        if(tipo==1)
            divisionImagenes1(imagenUno, imagenDos);
        else
            divisionImagenes2(imagenUno, imagenDos);
    }
    
    private void divisionImagenes1(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];
        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        temp = (int) ((255 - (255 * Math.min(imagenUno.getMatrizImg_R()[j][i], imagenDos.getMatrizImg_R()[j][i])))
                                / (Math.max(imagenUno.getMatrizImg_R()[j][i], imagenDos.getMatrizImg_R()[j][i]) + 1));
                        OP_R[j][i] = temp;
                    }
                    if (c == 1) {
                        temp = (int) ((255 - (255 * Math.min(imagenUno.getMatrizImg_G()[j][i], imagenDos.getMatrizImg_G()[j][i])))
                                / (Math.max(imagenUno.getMatrizImg_G()[j][i], imagenDos.getMatrizImg_G()[j][i]) + 1));
                        OP_G[j][i] = temp;
                    }
                    if (c == 2) {
                        temp = (int) ((255 - (255 * Math.min(imagenUno.getMatrizImg_B()[j][i], imagenDos.getMatrizImg_B()[j][i])))
                                / (Math.max(imagenUno.getMatrizImg_B()[j][i], imagenDos.getMatrizImg_B()[j][i]) + 1));
                        OP_B[j][i] = temp;
                    }
                }
            }
        }
    }

    private void divisionImagenes2(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        temp = (int) ((255 / (Math.log(256)))
                                * Math.log((1 + Math.max(imagenUno.getMatrizImg_R()[j][i], imagenDos.getMatrizImg_R()[j][i]))
                                / (Math.min(imagenUno.getMatrizImg_R()[j][i], imagenDos.getMatrizImg_R()[j][i]) + 1)));
                        OP_R[j][i] = temp;
                    }
                    if (c == 1) {
                        temp = (int) ((255 / (Math.log(256)))
                                * Math.log((1 + Math.max(imagenUno.getMatrizImg_G()[j][i], imagenDos.getMatrizImg_G()[j][i]))
                                / (Math.min(imagenUno.getMatrizImg_G()[j][i], imagenDos.getMatrizImg_G()[j][i]) + 1)));
                        OP_G[j][i] = temp;
                    }
                    if (c == 2) {
                        temp = (int) ((255 / (Math.log(256)))
                                * Math.log((1 + Math.max(imagenUno.getMatrizImg_B()[j][i], imagenDos.getMatrizImg_B()[j][i]))
                                / (Math.min(imagenUno.getMatrizImg_B()[j][i], imagenDos.getMatrizImg_B()[j][i]) + 1)));
                        OP_B[j][i] = temp;
                    }
                }
            }
        }
    }
}
