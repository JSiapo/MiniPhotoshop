/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.OpAritemticas;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Suma extends OpAritmeticas{
    
    public Suma(Imagen imagenUno, Imagen imagenDos,int tipo) {
        super(imagenUno, imagenDos);
        if(tipo==1)
        sumaImagenes1(imagenUno, imagenDos);
        else
        sumaImagenes2(imagenUno, imagenDos);
    }
    private void sumaImagenes1(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {

                        temp = imagenUno.getMatrizImg_R()[j][i] + imagenDos.getMatrizImg_R()[j][i];
                        if (temp >= 255) {
                            OP_R[j][i] = 255;
                        } else {
                            OP_R[j][i] = temp;
                        }
                    }
                    if (c == 1) {
                        temp = imagenUno.getMatrizImg_G()[j][i] + imagenDos.getMatrizImg_G()[j][i];
                        if (temp >= 255) {
                            OP_G[j][i] = 255;
                        } else {
                            OP_G[j][i] = temp;
                        }
                    }
                    if (c == 2) {
                        temp = imagenUno.getMatrizImg_B()[j][i] + imagenDos.getMatrizImg_B()[j][i];
                        if (temp >= 255) {
                            OP_B[j][i] = 255;
                        } else {
                            OP_B[j][i] = temp;
                        }
                    }
                }
            }
        }
    }

    private void sumaImagenes2(Imagen imagenUno, Imagen imagenDos) {
        int altoImgUno = imagenUno.getFilas();
        int anchoImgUno = imagenUno.getColumnas();

        int altoImgDos = imagenDos.getFilas();
        int anchoImgDos = imagenDos.getColumnas();

        int altoNuevo = Math.min(altoImgUno, altoImgDos);
        int anchoNuevo = Math.min(anchoImgUno, anchoImgDos);

        int temp;

        OP = null;
        OP_R = null;
        OP_G = null;
        OP_B = null;

        OP = new int[altoNuevo][anchoNuevo];
        OP_R = new int[altoNuevo][anchoNuevo];
        OP_G = new int[altoNuevo][anchoNuevo];
        OP_B = new int[altoNuevo][anchoNuevo];

        float alpha = 0.4f;
        float betha = 0.6f;

        for (int j = 0; j < altoNuevo; j++) {
            for (int i = 0; i < anchoNuevo; i++) {
                for (int c = 0; c < 3; c++) {
                    if (c == 0) {
                        temp = (int) (alpha * imagenUno.getMatrizImg_R()[j][i] + betha * imagenDos.getMatrizImg_R()[j][i]);
                        OP_R[j][i] = temp;
                    }
                    if (c == 1) {
                        temp = (int) (alpha * imagenUno.getMatrizImg_G()[j][i] + betha * imagenDos.getMatrizImg_G()[j][i]);
                        OP_G[j][i] = temp;
                    }
                    if (c == 2) {
                        temp = (int) (alpha * imagenUno.getMatrizImg_B()[j][i] + betha * imagenDos.getMatrizImg_B()[j][i]);
                        OP_B[j][i] = temp;
                    }
                }
            }
        }

    }
}
