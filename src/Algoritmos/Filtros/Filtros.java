/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Filtros;

import Algoritmos.Imagen;
import java.util.ArrayList;

/**
 *
 * @author jose
 */
public class Filtros {

    Imagen imagen;
    protected int[][] filtro_r;
    protected int[][] filtro_g;
    protected int[][] filtro_b;
    protected int[][] filtro;

    public Filtros(Imagen imagen) {
        this.imagen = imagen;
    }

    public int[][] getFiltro_r() {
        return filtro_r;
    }

    public int[][] getFiltro_g() {
        return filtro_g;
    }

    public int[][] getFiltro_b() {
        return filtro_b;
    }

    public int[][] getFiltro() {
        return filtro;
    }

    /*
     * 1 -> mediana
     * 2 -> minimo
     * 3 -> maximo
     * */
    protected void pintar(ArrayList vector, int indx_i, int indx_j, int n, int canal, int tipo_filtro) {
        for (int i = indx_i - n / 2; i <= indx_i + (n / 2); i++) {
            for (int j = indx_j - n / 2; j <= indx_j + (n / 2); j++) {
                if (canal == 1) {
                    filtro_r[i][j] = tipo_filtro == 1 ? (int) vector.get((vector.size() / 2) + 1) : tipo_filtro == 2 ? (int) vector.get(0) : (int) vector.get(vector.size() - 1);
                }
                if (canal == 2) {
                    filtro_g[i][j] = tipo_filtro == 1 ? (int) vector.get((vector.size() / 2) + 1) : tipo_filtro == 2 ? (int) vector.get(0) : (int) vector.get(vector.size() - 1);
                }
                if (canal == 3) {
                    filtro_b[i][j] = tipo_filtro == 1 ? (int) vector.get((vector.size() / 2) + 1) : tipo_filtro == 2 ? (int) vector.get(0) : (int) vector.get(vector.size() - 1);
                }
            }
        }
    }

    /*
     * 1 -> rojo
     * 2 -> verde
     * 3 -> azul
     * */
    
    protected ArrayList<Integer> asignarValores(int indx_i, int indx_j, int n, int canal) {
        ArrayList<Integer> vector = new ArrayList<>();
        int aux = 0;
        for (int i = indx_i - n / 2; i <= indx_i + (n / 2); i++) {
            for (int j = indx_j - n / 2; j <= indx_j + (n / 2); j++) {
                aux = canal == 1 ? imagen.getMatrizImg_R()[i][j] : canal == 2 ? imagen.getMatrizImg_G()[i][j] : imagen.getMatrizImg_B()[i][j];
                vector.add(aux);
            }
        }
        return vector;
    }

}
