/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Filtros;

import Algoritmos.Imagen;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author jose
 */
public class Maximo extends Filtros{
    
    public Maximo(Imagen imagen,int n) {
        super(imagen);
        maximo(n);
    }
    
    private void maximo(int n){
        filtro_r=null;
        filtro_g=null;
        filtro_b=null;

        filtro_r = new int[imagen.getFilas()][imagen.getColumnas()];
        filtro_g = new int[imagen.getFilas()][imagen.getColumnas()];
        filtro_b = new int[imagen.getFilas()][imagen.getColumnas()];

        ArrayList<Integer> vectorR=new ArrayList<Integer>();
        ArrayList<Integer> vectorG=new ArrayList<Integer>();
        ArrayList<Integer> vectorB=new ArrayList<Integer>();

        for (int i = n/2; i < imagen.getFilas() - n/2; i++)
            for (int j = n / 2; j < imagen.getColumnas() - n / 2; j++) {
                vectorR = asignarValores(i, j, n, 1);
                Collections.sort(vectorR);
                vectorG = asignarValores(i, j, n, 2);
                Collections.sort(vectorG);
                vectorB = asignarValores(i, j, n, 3);
                Collections.sort(vectorB);
                pintar(vectorR,i,j,n,1,3);
                pintar(vectorG,i,j,n,2,3);
                pintar(vectorB,i,j,n,3,3);
            }
    }
}
