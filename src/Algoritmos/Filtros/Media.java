/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos.Filtros;

import Algoritmos.Imagen;

/**
 *
 * @author jose
 */
public class Media extends Filtros {

    public Media(Imagen imagen, int n) {
        super(imagen);
        media(n);
    }

    private void rellenar_filtro(){
        for (int i = 0; i < imagen.getFilas(); i++)
            for (int j = 0; j < imagen.getColumnas(); j++) {
                filtro_r[i][j]=imagen.getMatrizImg_R()[i][j];
                filtro_g[i][j]=imagen.getMatrizImg_G()[i][j];
                filtro_b[i][j]=imagen.getMatrizImg_B()[i][j];
            }
    }
    
    private void media(int n) {
        filtro_r = null;
        filtro_g = null;
        filtro_b = null;

        filtro_r = new int[imagen.getFilas()][imagen.getColumnas()];
        filtro_g = new int[imagen.getFilas()][imagen.getColumnas()];
        filtro_b = new int[imagen.getFilas()][imagen.getColumnas()];
        rellenar_filtro();
        for (int i = n / 2; i < imagen.getFilas() - n / 2; i++) {
            for (int j = n / 2; j < imagen.getColumnas() - n / 2; j++) {
                pintarMedia(i, j, n, 1);
                pintarMedia(i, j, n, 2);
                pintarMedia(i, j, n, 3);
            }
        }
    }

    private void pintarMedia(int indx_i, int indx_j, int n, int canal) {
        int aux=0;
        for (int i = indx_i - n / 2; i <= indx_i + (n / 2); i++) {
            for (int j = indx_j - n / 2; j <= indx_j + (n / 2); j++) {
                if (i != indx_i && j != indx_j) {
                  aux=canal==1?aux+imagen.getMatrizImg_R()[i][j]:canal==2?aux + imagen.getMatrizImg_G()[i][j]:aux+ imagen.getMatrizImg_B()[i][j];
                }
            }
        }
        switch (canal) {
            case 1:
                filtro_r[indx_i][indx_j] = aux / (n * n);
                break;
            case 2:
                filtro_g[indx_i][indx_j] = aux / (n * n);
                break;
            default:
                filtro_b[indx_i][indx_j] = aux / (n * n);
                break;
        }
    }
}
